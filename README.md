# About
A pair of scripts for managing packages on Debian-based hosts (server or desktop).  
`initial_install.sh` installs comman applications and should be run once  
`update.sh` updates all packages and can be run regularly.

## Notes
Comment/uncomment desired package groups in `initial_install.sh` to get the configuration you want (server vs desktop, bare bones vs full featured)  

Be sure to “_chmod 755 update.sh_” so the file can be executed.
